#! /usr/bin/env python3

## Copyright (C) 2019 Richard Horridge
##
## This file is part of html-to-bibtex - Extraction of a Bibtex entry
## from a URL.
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.

import sys
import os
import requests
import subprocess
## For Python 3.6.x support
from subprocess import PIPE
import re
from datetime import datetime
from dateutil import parser
from bs4 import BeautifulSoup
from shutil import copyfile

def error (msg):
    """Print MSG as a string to stderr, followed by a trailing newline."""
    sys.stderr.write (str (msg) + "\n")

def publish_date (url, dump, html):
    """Determine the date on which URL was published. DUMP is a text dump
    of URL. HTML is a parsed html version produced using BeautifulSoup."""
    timefmt = "%Y%m%d%H%M%S"
    regex_search = re.search ("(?i)published", dump)
    if regex_search:
        return datetime.strptime ("19900101000000", timefmt)
    else:
        time_search = html.find ("time")
        if time_search:
            return parser.parse (time_search.get_text ())
        else:
            resolved_url = subprocess.run (["curl", "-Ls", "-o", "/dev/null",
                                        "-w", "%{url_effective}",
                                        "https://web.archive.org/web/" + url],
                                       stdout=PIPE, stderr=PIPE, encoding="utf-8")
            regex_match = re.match (".*web/([0-9]+)/http.*", resolved_url.stdout)
            if regex_match:
                datetime_num = regex_match.group(1)
                return datetime.strptime (datetime_num, timefmt)
            else:
                error ("Error: URL not found on web.archive.org")
                exit (1)


def page_title (webpage):
    """Determine the title of a parsed html WEBPAGE.

    Parsing should be done using BeautifulSoup."""
    return webpage.find ("title").get_text ()

def title_and_author (page_title):
    """Determine the title and author for a BibTex entry
    from the webPAGE_TITLE."""
    split_title = webpage_title.split("|")
    if len (split_title) == 1:
        split_title = webpage_title.split("-")
        if len (split_title) == 1:
            bibtex_author = ""
        else:
            bibtex_author = split_title[1]
    else:
        bibtex_author = split_title[1]

    bibtex_title = split_title[0]
    return (bibtex_title, bibtex_author)


def page_journal (webpage):
    """Determine the Journal of a parsed html WEBPAGE.

    Parsing should be done using BeautifulSoup."""
    site_name = webpage.find (attrs={"property":"og:site_name"})
    if site_name:
        return site_name["content"]
    rights = webpage.find (attrs={"name":"rights"})
    if rights:
        return rights["content"]
    return ""


if (len (sys.argv) < 2):
    error ("Usage:", str (sys.argv[0]), '<url>')
    sys.exit (1)

url = sys.argv[1]

if (len (sys.argv) >= 3):
    htmldir = sys.argv[2]
else:
    htmldir = "."


webpage = requests.get (url)
if webpage.status_code != 200:
    error ("Error: GET request unsuccessful")
    exit (1)
else:
    parsed_page = BeautifulSoup (webpage.content, "html.parser")

## Write HTML to file
bibfile = "/tmp/html_to_bibtex.html"
fp = open (bibfile, "w")
fp.write (parsed_page.prettify ())
fp.close ()

links_dump = subprocess.run (["links", "-dump", bibfile],
                             stdout=PIPE, stderr=PIPE,
                             encoding="utf-8")

published_date = publish_date (url, str (links_dump.stdout), parsed_page)

with open (bibfile, 'r') as fp:
    html = fp.read ()

webpage_title = page_title (parsed_page).strip ()

title_author = title_and_author (webpage_title)

webpage_journal = page_journal (parsed_page)

local_path = os.path.join (htmldir, webpage_title + ".html")
print (local_path)
copyfile (bibfile, local_path)


print ("""
@misc{{label,
    URL     = {{{0}}},
    JOURNAL = {{{1}}},
    AUTHOR  = {{{2}}},
    TITLE   = {{{3}}},
    YEAR    = {{{4}}},
    MONTH   = {{{5}}},
    NOTE    = {{Online; posted {6}; accessed {7}}},
}}
@comment{{File at "{8}"}}

""".format (url, webpage_journal.strip(), title_author[1].strip(),
            title_author[0].strip(),
            published_date.year, published_date.month,
            published_date.strftime ("%Y-%b-%d"),
            datetime.now().strftime ("%Y-%b-%d"),
            local_path))
